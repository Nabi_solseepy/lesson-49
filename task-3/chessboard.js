var chessboard = '';
for(var i = 0; i < 8; i++) {
    for(var j = 1; j <= 8; j++) {
        if((j + i) % 2 === 0){
            chessboard += '██';
        } else {
            chessboard  += '  ';
        }
    }
    chessboard += '\n';
}
console.log(chessboard);